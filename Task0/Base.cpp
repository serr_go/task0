#pragma warning(disable : 4996)
#include "stdafx.h"
#include "Base.h"

Base::Base(){}

Base::Base(ifstream& file)
{
	string tmpLine;
	int line = 0;
	char c = ';';

	while (getline(file, tmpLine))
	{
		fillBase(splitStr(tmpLine, ';'), line++);
	}
}

vector<Base::DataBase> Base::getBase()
{
	return base;
}

void Base::printBase(vector<DataBase> tmp)
{
	cout.precision(20);
	for (auto dtBs : tmp)
	{
		cout << dtBs.days << "; "
			<< dtBs.shopName << "; "
			<< dtBs.item << "; "
			<< dtBs.cardNum << "; "
			<< dtBs.price
			<< endl;
	}
	cout << endl;
}

void Base::sortBase(int fieldNum)
{
	switch (fieldNum)
	{
	case 0:
	{
		sort(base.begin(), base.end(), [](const DataBase& a, const DataBase& b) {
			return a.days > b.days;
		});
		break;
	}
	case 2:
	{
		sort(base.begin(), base.end(), [](const DataBase& a, const DataBase& b) {
			return a.cardNum < b.cardNum;
		});
		break;
	}
	default:
		break;
	}
}

void Base::sumPrice()
{
	vector<DataBase> tempBase = base;
	cout.precision(20);

	sort(tempBase.begin(), tempBase.end(), [](const DataBase& a, const DataBase& b) {
		return a.cardNum < b.cardNum;
	});

	auto last = unique(tempBase.begin(), tempBase.end(), [](DataBase& a, const DataBase& b) {
		if (a.cardNum == b.cardNum)
			a.price += b.price;
		return a.cardNum == b.cardNum;
	});
	tempBase.erase(last, tempBase.end());

	for (auto n : tempBase)
	{
		cout << n.cardNum << "; "<< n.price << endl;
	}
	cout << endl;
}

vector<string> Base::splitStr(string str, char const frstSpltr, char secSpltr, char thrdSpltr)
{
	string buff{ "" };
	vector<string> vLine;

	for (auto n : str)
	{
		if ((n != frstSpltr) && (n != secSpltr) && (n != thrdSpltr))
			buff += n;
		else
			if (((n == frstSpltr) || (n == secSpltr) || (n == thrdSpltr)) && buff != "")
			{
				vLine.push_back(buff);
				buff = "";
			}
	}
	if (buff != "")
		vLine.push_back(buff);

	return vLine;
}

int Base::getDateDifference(vector<string> str, int wrd)
{
	int year;
	vector<string> splitedData;
	time_t t = time(0);   
	struct tm * now = localtime(&t);

	splitedData = splitStr(str[wrd], '.', ' ', ':');

	year = stoi(splitedData[2]) + 100;

	struct tm filelDate = { 0,stoi(splitedData[4]),stoi(splitedData[3]),stoi(splitedData[0]),stoi(splitedData[1]),year };
	struct tm currentDate = { 0,now->tm_min,now->tm_hour,now->tm_mday,now->tm_mon + 1,now->tm_year };

	time_t x = mktime(&filelDate);
	time_t y = mktime(&currentDate);

	if (x != (time_t)(-1) && y != (time_t)(-1))
	{
		return  difftime(y, x) / (60 * 60 * 24);
	}
}

void Base::fillBase(vector<string> str, int i)
{
	int word = 0;
	base.resize(base.size() + 1);

	base[i].days = getDateDifference(str,word++);
	base[i].shopName = str[word++];
	base[i].item = str[word++];
	base[i].cardNum = stod(str[word++]);
	base[i].price = stoi(str[word++]);
}