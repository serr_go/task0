#pragma once
#pragma warning(disable : 4996)
#include "stdafx.h"
#include <regex>
#include <iterator>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>
#include <chrono>
using namespace std;

class Base
{
protected:
	struct DataBase
	{
		int days,
			price;
		double cardNum;
		string shopName,
			item;
	};
private:
	vector<DataBase> base;
	vector<string> splitStr(string str,char frstSpltr, char secSpltr = ';', char thrdSpltr = ';');
	void fillBase(vector<string> s, int i);
	int getDateDifference(vector<string> str, int wrd);
public:

	Base();
	Base(ifstream& file);

	vector<DataBase> getBase();

	void printBase(vector<DataBase> tmp);
	void sortBase(int fieldNum = 0);
	void sumPrice();
};