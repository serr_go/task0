#include "stdafx.h"
#include "Base.h"
#include "SaleBase.h"

SaleBase::SaleBase(Base obj)
{
	base = obj.getBase();
}

void SaleBase::countSale()
{
	for (int i = 0; i < base.size(); i++)
	{
		if (base[i].price > 100)
		{
			base[i].price *= 0.95;
		}
	}
}

vector<Base::DataBase> SaleBase::getBase()
{
	return base;
}