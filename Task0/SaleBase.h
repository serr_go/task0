#pragma once
#include "stdafx.h"
#include "Base.h"

class SaleBase : public Base
{
	vector<DataBase> base;
public:
	SaleBase(Base obj);
	vector<DataBase> getBase();
	void countSale();
};