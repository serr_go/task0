#include "stdafx.h"
#include "Base.h"
#include "SaleBase.h"
using namespace std;

int main()
{
	string fileName;

	cout << "Input file name:";
	cin >> fileName;

	ifstream file(fileName);
	if (!file)
	{
		cout << "File not opend\n";
		return -1;
	}

	Base DataList(file);
	file.close();

	DataList.printBase(DataList.getBase());
	DataList.sortBase();
	DataList.printBase(DataList.getBase());
	DataList.sortBase(2);
	DataList.printBase(DataList.getBase());
	DataList.sumPrice();
	SaleBase BaseWthSales(DataList);
	BaseWthSales.countSale();
	BaseWthSales.printBase(BaseWthSales.getBase());

	system("pause");
    return 0;
}
